package com.testtech.configTest;

import org.testng.annotations.Test;
import com.github.wasiqb.coteafs.config.loader.ConfigFactory;

public class ConfigFileTests {

	@Test(enabled = false)
	public void configPropertiesTest() {

		ConfigFactory properties = ConfigFactory.newInstance().withDefault("config.properties").load();

		System.out.println(properties.getString("appurl"));
		System.out.println(properties.getString("port"));
		System.out.println(properties.getString("username"));
		System.out.println(properties.getString("password"));

	}

	@Test(enabled = true)
	public void jsonTest() {

		ConfigFactory json = ConfigFactory.newInstance().withDefault("NewData.json").load();
				
		System.out.println(json.getString("org"));
		System.out.println(json.getString("brachname"));

	}
	
	@Test
	public void yamlTest () {
		ConfigFactory json = ConfigFactory.newInstance().withDefault("yaml-data.yaml").load();
		System.out.println(json.getString("signup.email"));
		System.out.println(json.getString("signup.pass"));
		System.out.println(json.getString("signup"));
	}

	
}
